$(document).ready(function () {
    var data, dataOne, dataTwo, dataThree;

    var origin1 = $.getJSON('http://s3.amazonaws.com/logtrust-static/test/test/data1.json');
    var origin2 = $.getJSON('http://s3.amazonaws.com/logtrust-static/test/test/data2.json');
    var origin3 = $.getJSON('http://s3.amazonaws.com/logtrust-static/test/test/data3.json');


    function loadCharts(dataPieChart, dataPointsChart) {

        $('#chartPoints').highcharts({
            title: {
                text: 'Chart',
                x: -20
            }, xAxis: {
                type: 'datetime',
                "labels": {
                    "format": "{value:%Y-%m-%d}"
                },
                categories: (function () {
                    var categories = [];
                    $.each(dataPointsChart, function (index, item) {
                        categories.push(index);
                    });
                    return categories;
                })(),
            },
            yAxis: {
                title: {
                    text: 'Value'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            tooltip: {
                headerFormat: '<b>{series.name}</b><br>',
                pointFormat: '{point.x:%d-%m-%Y}: {point.y:.2f}'
            },
            series: (function () {
                // generate an array of random data
                var series = [];
                $.each(dataPointsChart, function (index, item) {
                    var data = [];
                    $.each(item, function (key, value) {
                        data.push({x: value['d'], y: value['val']});
                    });
                    series.push({
                        'name': index,
                        'data': data
                    });
                });
                return series;
            })()
        });

        $('#pieChart').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Total Categories'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            series: [{
                name: 'Categories',
                colorByPoint: true,
                data: (function () {
                    // generate an array of random data
                    var data = [];
                    $.each(dataPieChart, function (index, item) {
                        data.push({
                            'name': item['type'],
                            'y': item['val']
                        });
                    });
                    return data;
                })()
            }]
        });
    }

    $.when(origin1, origin2, origin3).then(function (data1, data2, data3) {
        var data = data1[0].concat(data2[0]).concat(data3[0]);
        _.each(data, function (point) {
            _.each(point, function (value, key) {
                // Data 1
                if (key == 'cat') {
                    point['category'] = point['cat'].toUpperCase();
                }
                if (key == 'value') {
                    point['val'] = point['value'];
                }
                // Data 2
                if (key == 'myDate') {
                    point['d'] = moment(point['myDate'], "YYYY-MM-DD").valueOf();
                } else if (key == 'categ') {
                    point['category'] = point['categ'].toUpperCase();
                }
                // Data 3
                if (key == 'raw') {
                    var regexDate = /[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])/;
                    var res = point['raw'].match(regexDate);
                    point['d'] = moment(res[0], "YYYY-MM-DD").valueOf();
                    var regexCat = /\#(.*?)\#/;
                    point['category'] = point['raw'].match(regexCat)[1].toUpperCase();
                }
            });
        });
        var sorted = _.sortBy(data, function (o) {
            return o.d;
        })
        var groups = _(sorted).groupBy('category');
        var totalByCategory = _(groups).map(function (g, key) {
            return {
                type: key,
                val: _(g).reduce(function (m, x) {
                    return m + x.val;
                }, 0)
            };
        });
        loadCharts(totalByCategory, groups);
    });

});