PS_LG
=====

This repository contains two problems. The first one has been made with Python and the second one with Javascript.

The libraries used in the second exercise have been Highcharts, jQuery, moment (for date management) and Underscore (for array management and filter).