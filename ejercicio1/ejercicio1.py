def divisors(number):
	limit = int(number/2 + 1)
	divisors = [1]
	for i in range(2, limit):
		if (number % i == 0) :
			divisors.append(i)
	return divisors


def classify(number, divisors):
	if (number == 0):
		print "The number 0 is not valid"
	elif (sum(divisors) < number or number == 1):
		print "The number" , number , "is deficient"
	elif (sum(divisors) == number):
		print "The number" , number , "is perfect"
	elif (sum(divisors) > number):
		print "The number" , number , "is abundant"

if __name__ == "__main__":
	numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 28, 496, 8128]
	for number in numbers:
		classify(number, divisors(number))
